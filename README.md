# Xv6 Enhancement Project

## Goal
* Enhanced and modern userland development, with C standard libraries and POSIX functionalities available.
* Clean and comprehensive kernel code file structures.
* New features like networking, socket and IPC APIs.

## Project Structure
This project is divided into three parts:
* [Libxv6](src/libxv6/README.md): Xv6 system library
* [Userland Utilities](src/userland-utils/README.md): Xv6 userland utilties
* [Kernel Extensions](src/kernel-extra/README.md): Xv6 kernel networking & socket extensions

## License
Header files in [`include/newlib`](include/newlib) are subjected to [Newlib license](https://sourceware.org/newlib/COPYING.NEWLIB).  
All other files in this project are licensed under [GNU GPLv3](LICENSE).
