# Xv6 Kernel Networking Extension
This project aims to add networking and IPC functionalities to xv6.

## Project Structure
* Kernel Infrastructure
  - Memory Pool System
  - Kernel Init Hook System
  - Kernel Definitions & Utility Functions
* Networking
  - Socket System Call & Protocol Agnostic Layer
  - Unix Domain Datagram Socket Module
  - UDP Socket Module
  - Local Network Interface
  - Kernel IP Routing Module

## APIs
Most of the APIs in this project are documented in Javadoc-like format.  
Refer to source code and comments for API descriptions.
