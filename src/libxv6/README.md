# Libxv6
Libxv6 is a system library which provides extra features not included in Newlib and conforms to POSIX specification and Single Unix Specification as much as possible.

## Build
To build the library, `cd` into `src` folder and then `make` the library.  
You might then need to update the library file in `xv6-extra/lib` by running `make sync`.

## APIs
See [The Open Group POSIX Specification](http://pubs.opengroup.org/onlinepubs/9699919799/).

## Notes
* Currently only network and socket related APIs are implemented.
* Function `inet_addr` is not implemented and function `inet_ntoa` is not implemented correctly, so currently these two functions cannot be used.
