/** @file
 * @brief Program entry for Xv6 C programs
 * @author Qifan Lu
 * @date April 11, 2016
 * @version 1.0.0
 */

//[ Header Files ]
//Newlib
#include <stdlib.h>

//[ Functions ]
//Main function (In user program)
extern int main(int argc, char** argv);

/**
 * Program entry function
 *
 * @param argc Arguments amount
 * @param argv Arguments
 */
void _start(int argc, char** argv)
{   exit(main(argc, argv));
}
