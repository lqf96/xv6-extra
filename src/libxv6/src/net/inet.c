/** @file
 * @brief Implementation for basic internet operations.
 * @author Qifan Lu
 * @date April 11, 2016
 * @version 1.0.0
 */

//[ Includes ]
//Libxv6
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <stddef.h>
#include <netinet/in.h>

//[ Variables ]
/**
 * IPv4 string buffer
 */
static char ipv4_str_buffer[INET_ADDRSTRLEN];

//[ Functions ]
/**
 * Swap bytes (16-bit number)
 */
static void swaps(uint16_t* num)
{   char* _num = (char*)num;
    uint8_t tmp;

    tmp = _num[0];
    _num[0] = _num[1];
    _num[1] = tmp;
}

/**
 * Swap bytes (32-bit number)
 */
static void swapl(uint32_t* num)
{   char* _num = (char*)num;
    uint8_t tmp;

    tmp = _num[0];
    _num[0] = _num[3];
    _num[3] = tmp;

    tmp = _num[1];
    _num[1] = _num[2];
    _num[2] = tmp;
}

//Convert from local endianness to network endianness (32-bit)
uint32_t htonl(uint32_t hostlong)
{   swapl(&hostlong);
    return hostlong;
}

//Convert from local endianness to network endianness (16-bit)
uint16_t htons(uint16_t hostshort)
{   swaps(&hostshort);
    return hostshort;
}

//Convert from network endianness to local endianness (32-bit)
uint32_t ntohl(uint32_t netlong)
{   swapl(&netlong);
    return netlong;
}

//Convert from network endianness to local endianness (16-bit)
uint16_t ntohs(uint16_t netshort)
{   swaps(&netshort);
    return netshort;
}

//Convert IPv4 string to 32-bit network endianness IPv4 address (Implementation)
static in_addr_t inet_addr_impl(const char* addr_str, bool* success)
{
    //TODO: Implement this function
    *success = false;
    return 0xffffffff;
}

//Convert IPv4 string to 32-bit network endianness IPv4 address (Wrapper)
in_addr_t inet_addr(const char* addr_str)
{   bool success;
    in_addr_t result = inet_addr_impl(addr_str, &success);

    //Failed
    if (!success)
        result = 0xffffffff;
    return result;
}

//Convert a 32-bit network endianness IPv4 address to a string
char* inet_ntoa(in_addr_t addr)
{   uint8_t* _addr = (uint8_t*)(&addr);
    int j = INET_ADDRSTRLEN-1;

    for (uint8_t i=0;i<4;i++)
    {   uint8_t current_num = _addr[3-i];
        //Print delimiter
        ipv4_str_buffer[j] = (i==0)?'\0':'.';
        j--;
        //Print number
        while (current_num!=0)
        {   ipv4_str_buffer[j] = current_num%10+'0';
            current_num /= 10;
            j--;
        }
    }

    return ipv4_str_buffer+j+1;
}

//Convert numeric internet address to a string
const char* inet_ntop(int af, const void* src, char* dst, socklen_t size)
{   //IPv4
    if (af==AF_INET)
    {   //Buffer too small
        if (size<INET_ADDRSTRLEN)
        {   errno = ENOMEM;
            return NULL;
        }

        char* result = inet_ntoa(*((const in_addr_t*)src));
        memmove(dst, result, INET_ADDRSTRLEN);
        return dst;
    }
    //Not implemented; not supported
    else
    {   errno = EAFNOSUPPORT;
        return NULL;
    }
}

//Convert a network address string to a numeric internet address
int inet_pton(int af, const char* src, void* dst)
{   //IPv4
    if (af==AF_INET)
    {   bool success;
        in_addr_t result = inet_addr_impl(src, &success);

        //Failed
        if (!success)
            return 0;
        //Succeeded
        memmove(dst, &result, sizeof(in_addr_t));
        return 1;
    }
    //Not implemented; not supported
    else
    {   errno = EAFNOSUPPORT;
        return -1;
    }
}
