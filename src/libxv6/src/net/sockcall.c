/** @file
 * @brief Xv6 POSIX socket API implementation.
 * @author Qifan Lu
 * @date April 11, 2016
 * @version 1.0.0
 */

//[ Header Files ]
//Libxv6
#include <sys/socket.h>
#include <sys/syscall.h>
#include <errno.h>

//[ Types ]
/**
 * Socket creation parameters
 */
struct __sockcall_create_param
{   int domain;
    int type;
    int protocol;
};

/**
 * Socket bind parameters
 */
struct __sockcall_bind_param
{   const struct sockaddr* address;
    socklen_t address_len;
};

/**
 * Socket send parameters
 */
struct __sockcall_send_param
{   const void* message;
    size_t length;
    int flags;
    const struct sockaddr* dest_addr;
    socklen_t dest_len;
};

/**
 * Socket receive parameters
 */
struct __sockcall_recv_param
{   void* buffer;
    size_t length;
    int flags;
    struct sockaddr* address;
    socklen_t* address_len;
};

//[ Constants ]
//* Socket system call ID
/**
 * Create
 */
#define XV6_SOCKCALL_CREATE 0
/**
 * Bind
 */
#define XV6_SOCKCALL_BIND 1
/**
 * Send
 */
#define XV6_SOCKCALL_SEND 2
/**
 * Receive
 */
#define XV6_SOCKCALL_RECV 3

//[ Functions ]
//Socket: Create socket for network communication
int socket(int domain, int type, int protocol)
{   //Create parameter object
    struct __sockcall_create_param param = {
        .domain = domain,
        .type = type,
        .protocol = protocol
    };
    //Do socket system call
    int result = xv6_sys_sockcall(0, XV6_SOCKCALL_CREATE, &param);

    //Success
    if (result>=0)
        return result;
    //Failed
    else
    {   errno = -1*result;
        return -1;
    }
}

//Bind given socket to given address
int bind(int socket, const struct sockaddr* address, socklen_t address_len)
{   //Create parameter object
    struct __sockcall_bind_param param = {
        .address = address,
        .address_len = address_len
    };
    //Do socket system call
    int result = xv6_sys_sockcall(socket, XV6_SOCKCALL_BIND, &param);

    //Success
    if (result==0)
        return 0;
    //Failed
    else
    {   errno = -1*result;
        return -1;
    }
}

//Send data to remote socket (No remote address given)
ssize_t send(int socket, const void* message, size_t length, int flags)
{   //Forward to "sendto" function
    return sendto(socket, message, length, flags, NULL, 0);
}

//Send data to remote socket, with remote address given
ssize_t sendto(int socket, const void* message, size_t length, int flags, const struct sockaddr* dest_addr, socklen_t dest_len)
{   //Create parameter object
    struct __sockcall_send_param param = {
        .message = message,
        .length = length,
        .flags = flags,
        .dest_addr = dest_addr,
        .dest_len = dest_len
    };
    //Do socket system call
    ssize_t result = xv6_sys_sockcall(socket, XV6_SOCKCALL_SEND, &param);

    //Success
    if (result>=0)
        return result;
    //Failed
    else
    {   errno = -1*result;
        return -1;
    }
}

//Receive data from remote socket, as well as remote address
ssize_t recv(int socket, void* buffer, size_t length, int flags)
{   //Forward to "recvfrom" function
    return recvfrom(socket, buffer, length, flags, NULL, NULL);
}

//Receive data from remote socket, as well as remote address
ssize_t recvfrom(int socket, void* buffer, size_t length, int flags, struct sockaddr* address, socklen_t* address_len)
{   //Create parameter object
    struct __sockcall_recv_param param = {
        .buffer = buffer,
        .length = length,
        .flags = flags,
        .address = address,
        .address_len = address_len
    };
    //Do socket system call
    ssize_t result = xv6_sys_sockcall(socket, XV6_SOCKCALL_RECV, &param);

    //Success
    if (result>=0)
        return result;
    //Failed
    else
    {   errno = -1*result;
        return -1;
    }
}
