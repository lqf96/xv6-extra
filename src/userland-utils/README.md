# Xv6 Userland Utilities

## Build
To build all programs on host system:
```bash
make all
```
To build all programs for xv6:
```bash
make -f Makefile.xv6 all
```
To build separate targets, simply change the target name to the name of each program.

## Utilities
### UDP Test
This program is written to test UDP functionalities on xv6.  
Upon running the program forks itself, after which the child process becomes the worker and the parent process becomes master. The master keeps reading from standard input and send what it reads to worker through UDP. The worker receives string from the master, inverts the string and sends it back to the master. Then the master displays the inverted string received from UDP socket.  
This program demostrates a simple yet useful workflow, in which a process sends computation works to another process, and wait for results to be sent back.
### Unix Test
This program is written to test Unix Domain Datagram Socket functionalities on xv6. It works similar to UDP Test.
### Xv6 Log Daemon
This program is a daemon that collects logs from Unix Domain Socket and writes them into a single file. It is expected to be a system service daemon that keeps running in background.
