/** @file
 * @brief Xv6 unix domain socket test program
 * @author Qifan Lu
 * @date June 23, 2016
 * @version 1.0.0
 */

//[ Header Files ]
//System headers
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

//[ Variables ]
//Master address ("./master.sock")
struct sockaddr_un master_addr = {
    .sun_family = AF_UNIX,
    .sun_path = "./master.sock"
};

//Worker socket ("./worker.sock")
struct sockaddr_un worker_addr = {
    .sun_family = AF_UNIX,
    .sun_path = "./worker.sock"
};

//[ Functions ]
//Invert string
void invert_str(char* str, unsigned int length)
{   for (unsigned int i=0;i<length/2;i++)
    {   char tmp = str[i];
        str[i] = str[length-1-i];
        str[length-1-i] = tmp;
    }
}

//Master side program
void master_proc()
{   //Wait for worker
    puts("Waiting for worker...");
    sleep(2);

    //Create socket
    int sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sock==-1)
    {   perror("[unix-test master] Socket creation error");
        exit(1);
    }
    //Bind to "./master.sock"
    int status = bind(sock, (struct sockaddr*)(&master_addr), sizeof(master_addr));
    if (status==-1)
    {   perror("[unix-test master] Bind error");
        exit(2);
    }

    //String buffer
    char buffer[100];
    //Master loop
    while (true)
    {   //Read string from input
        gets(buffer);
        //Send string to worker
        sendto(sock, buffer, strlen(buffer)+1, 0, (struct sockaddr*)(&worker_addr), sizeof(worker_addr));
        //Exit
        if (strcmp(buffer, "exit")==0)
            break;
        //Receive inverted string from worker
        recvfrom(sock, buffer, sizeof(buffer), 0, NULL, NULL);
        //Print string
        puts(buffer);
    }
}

//Worker side program
void worker_proc()
{   //Create Unix Domain socket
    int sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sock==-1)
    {   perror("[unix-test worker] Socket creation error");
        exit(1);
    }
    //Bind to "./worker.sock"
    int status = bind(sock, (struct sockaddr*)(&worker_addr), sizeof(worker_addr));
    if (status==-1)
    {   perror("[unix-test worker] Bind error");
        exit(2);
    }

    //String buffer
    char buffer[100];
    //Worker loop
    while (true)
    {   //Receive string from master
        recvfrom(sock, buffer, sizeof(buffer), 0, NULL, NULL);
        //Exit
        if (strcmp(buffer, "exit")==0)
            break;
        //Invert string
        invert_str(buffer, strlen(buffer));
        //Send inverted string
        sendto(sock, buffer, strlen(buffer)+1, 0, (struct sockaddr*)(&master_addr), sizeof(master_addr));
    }
}

//Program entry
int main(int argc, char** argv)
{   //Run worker side program
    if (fork()==0)
        worker_proc();
    //Run master side program
    else
        master_proc();

    return 0;
}
